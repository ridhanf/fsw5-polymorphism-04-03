// Parent Class
class Hewan {
  constructor(nama, kaki, alam) {
    this.nama = nama;
    this.kaki = kaki; // 0, 2, 4
    this.alam = alam; // darat, laut, udara, dua_alam
  }

  info () {
      console.log(`nama: ${this.nama}`);
      console.log(`kaki: ${this.kaki}`);
      console.log(`alam: ${this.alam}`);
  }

  perilaku () {
    console.log(`${this.constructor.name}:`, "Bertingkah!");
  }
}

// Module/Helper Hewan
const PublicHewan = Base => class extends Base {
  ciriKhas(){
    console.log(`${this.nama.nama} dan hewan lainnya juga mempunyai ciri masing-masing `);
  }
}

// Module/Helper Mamalia   -> Fadhil
const Mamalia = Base => class extends Base {
  menyusui() {
    console.log(`Hewan ini menyusui`);
  }
}

// Module/Helper Aves  -> Umam
const Aves = Base => class extends Base {
  sayap(){
    console.log(`Hewan ini memiliki sayap`)
  }
}
// Module/Helper Pisces   -> Kevin
const Pisces = Base => class extends Base {
  infoPisces() {
    console.log(`Kalau 1 ${this.nama.nama} , Kalo banyak jadi hiu hiu hiu `);
  }
}

// Module/Helper Reptilia -> Andre
const Reptilia = Base => class extends Base{
  infoReptil(){
    console.log(`${this.nama.nama} : mempunyai bisa yang sangat mematikan`); // Kobra Dari  Hewan Class
  }
}

// Child Class Amfibia  -> Bramantyo
const Amfibia = Base => class extends Base {
  metamorfosis() {
    console.log(`${this.nama} mengalami perubahan bentuk sejak menetas sampai dewasa`);
  }
}


// Child Class
// Child Class Kuda  -> Fadhil
class Kuda extends Mamalia(Hewan) {
  constructor(props) {
    super(props);
  }

  perilaku() {
    super.perilaku();
    this.menyusui();
  }
}

// Child Class Burung   -> Umam
class Burung extends Aves (Hewan){
  constructor (props){
    super(props);
  }

  perilaku() {
    super.perilaku();
    this.sayap();
  }
}

// Child Class Ikan -> Kevin
class Ikan extends PublicHewan(Pisces(Hewan)) {

  constructor(props) {
    super(props);
  }
  info() {
    super.info(); // Dari Hewan class
    super.perilaku(); // Dari Hewan class
    //   super.ciriKhas(); // Dari Public Hewan Class
    super.infoPisces(); // Dari Pisces Class
  }
}

// Child Class Reptil -> Andre
class Reptil extends PublicHewan(Reptilia(Hewan)){
 
  constructor (props){
    super(props);
  }
  info(){
    super.info(); // Dari Hewan class
    super.perilaku(); // Dari Hewan class
    super.ciriKhas(); // Dari Public Hewan Class
    super.infoReptil(); // Dari Reptilia Class
  }
}

// Child Class Katak    -> Bramantyo
class Katak extends Amfibia(Hewan) {
  constructor (nama, kaki, alam, tingkah) {
    super(nama, kaki, alam);
    this.tingkah = tingkah;
  }

  perilaku() {
    super.perilaku();
    for (let i = 0; i < 3; i++ ){
      console.log(`${this.tingkah}`);
      console.log('lompat\n');
    }
    this.metamorfosis();
  }
}




// Instanstiate
// Zebra        -> Fadhil
const Kuda1 = new Kuda({
  nama: "Zebra",
  kaki: 4,
  alam: "Darat"
})
Kuda1.menyusui()
Kuda1.perilaku();

// Merpati      -> Umam
const Burung1 = new Burung({
  nama : "Merpati",
  kaki : 2,
  alam: "Udara"
})

// Hiu          -> Kevin
const Hiu = new Ikan({
  nama: 'Hiu',
  kaki: 0,
  alam: 'Air'
})

// Hiu.perilaku();
Hiu.info();


// Kobra        -> Andre
const Kobra = new Reptil({nama:'Kobra',kaki:4,alam:'Air'})
// Kobra.perilaku();
Kobra.info();

// Katak Hijau  -> Bramantyo
const katakHijau = new Katak("Kodok Sawah", 4, "Dua Alam (darat dan air)", "KUNGKONG!");
katakHijau.info();
katakHijau.perilaku();