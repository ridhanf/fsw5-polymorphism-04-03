# Tugas Polymorphism - Binar FSW5 Chapter 04 Session 03

Sumber-sumber kode ini dibuat oleh:

[Fadhil](https://gitlab.com/ridhanf/)

[Bramantyo](https://gitlab.com/bramantyobk)

[Kevin](https://gitlab.com/kvnsng9)

[Umam](https://gitlab.com/muhammad_khaerul_umam)

[Andre](https://gitlab.com/sinabariba.andre)


## License

All source code in this repository is available jointly under the MIT License and the Beerware License. See
[LICENSE](LICENSE) for details.

## Langkah-langkah

Untuk mengunduh repository ini ke dalam komputer lokal:

```
$ git clone https://gitlab.com/ridhanf/fsw5-polymorphism-04-03
```

Untuk membuat branch baru:

```
$ git checkout -b [nama branch / nama fitur]
```

Jangan lupa untuk pindah ke branch masing-masing:

```
$ git checkout [nama branch / nama fitur]
```

Unggah setiap pekerjaan ke masing-masing branch:

```
$ git push origin [nama branch / nama fitur]
```

Satukan dengan branch development (develop):

```
$ git checkout develop
$ git merge [nama branch / nama fitur]
```

Untuk informasi lebih lanjut, hubungi
[WA](https://api.whatsapp.com/send?phone=6281220220697&text=&source=&data=).